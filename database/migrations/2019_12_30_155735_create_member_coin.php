<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberCoin extends Migration {

    public function up() {
        Schema::create('member_coin', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->decimal('total_coin', 8, 4);
            $table->smallInteger('coin_status')->default(0)->comment('0 = coin masuk, 1 => coin keluar, 2 => coin transfer (keluar)');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
            
            $table->index('user_id');
            $table->index('coin_status');
            $table->index('created_at');
            $table->index('deleted_at');
        });
    }

    public function down() {
        Schema::dropIfExists('member_coin');
    }
}
