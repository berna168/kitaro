<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUplineIdOnGenealogy extends Migration {

    public function up() {
        Schema::table('genealogy', function(Blueprint $table){
            $table->integer('upline_id')->nullable();
            $table->text('upline_detail')->nullable();
            
            $table->index('upline_id');
        });
    }

    public function down() {
        Schema::table('genealogy', function(Blueprint $table){
            $table->dropColumn('upline_id');
            $table->dropColumn('upline_detail');
        });
    }
}
