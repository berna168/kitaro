<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoinSettingOnMemberCoin extends Migration {

    public function up() {
        Schema::table('member_coin', function(Blueprint $table){
            $table->integer('coin_setting_id')->default(1);
            
            $table->index('coin_setting_id');
        });
    }

    public function down() {
        Schema::table('member_coin', function(Blueprint $table){
            $table->dropColumn('coin_setting_id');
        });
    }
}
