<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsPlanOnUsers extends Migration {

    public function up() {
        Schema::table('users', function(Blueprint $table){
            $table->smallInteger('plan')->default(1)->comment('1 = Plan A, 2 = Plan B');
            $table->smallInteger('stage')->default(1);
            $table->index('plan');
            $table->index('stage');
        });
    }

    public function down() {
        Schema::table('users', function(Blueprint $table){
            $table->dropColumn('plan');
            $table->dropColumn('stage');
        });
    }
}
