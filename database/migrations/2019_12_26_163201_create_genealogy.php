<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenealogy extends Migration {

    public function up() {
        Schema::create('genealogy', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('kiri_id')->nullable();
            $table->integer('tengah_id')->nullable();
            $table->integer('kanan_id')->nullable();
            $table->tinyInteger('is_active')->default(1)->comment('0 = tidak aktif, 1 = aktif');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
            
            $table->index('user_id');
            $table->index('kiri_id');
            $table->index('tengah_id');
            $table->index('kanan_id');
            $table->index('is_active');
            $table->index('created_at');
            $table->index('deleted_at');
        });
    }

    public function down() {
        Schema::dropIfExists('genealogy');
    }
}
