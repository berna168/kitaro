<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlanOnBonusMember extends Migration {

    public function up() {
        Schema::table('bonus_member', function(Blueprint $table){
            $table->smallInteger('plan')->default(1)->comment('1 = Plan A, 2 = Plan B');
            $table->index('plan');
        });
    }

    public function down() {
        Schema::table('bonus_member', function(Blueprint $table){
            $table->dropColumn('plan');
        });
    }
}
