<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCharityOnTransferWd extends Migration {

    public function up() {
        Schema::table('transfer_wd', function(Blueprint $table){
            $table->integer('charity');
            $table->index('charity');
        });
    }

    public function down() {
        Schema::table('transfer_wd', function(Blueprint $table){
            $table->dropColumn('charity');
        });
    }
}
