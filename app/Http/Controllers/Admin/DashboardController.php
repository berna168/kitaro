<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Pin;
use App\Model\Member;
use App\Model\Memberpackage;
use App\Model\Bonus;
use App\Model\Transferwd;
use App\Model\Package;
use App\Model\Bonussetting;
use App\Model\Sales;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller {

    public function __construct(){
        
    }
    
    public function getDashboard(){
        $dataUser = Auth::user();
        if(in_array($dataUser->user_type, array(10))){
            return redirect()->route('mainDashboard');
        }
        $modelPin = new Pin;
        $modelMember = new Member;
        $getTotalPin = $modelPin->getTotalPinAdmin();
        $getAllmember = $modelMember->getAllMember();
        return view('admin.home.admin-dashboard')
                    ->with('headerTitle', 'Dashboard')
                    ->with('dataPin', $getTotalPin)
                    ->with('totalMember', $getAllmember)
                    ->with('dataUser', $dataUser);
        
    }
    
    public function getMemberDashboard(){
        $dataUser = Auth::user();
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        $modelMemberPackage = New Memberpackage;
        $modelMember = new Member;
        $modelBonus = new Bonus;
        $modelBonusSetting = new Bonussetting;
        $modelWD = new Transferwd;
        $modelPackage = New Package;
        $modelPin = new Pin;
        $sponsor_id = $dataUser->sponsor_id;
        $dataSponsor = $modelMember->getUsers('id', $sponsor_id);
        if($dataUser->is_active == 0){
            $getCheckNewOrder = $modelMemberPackage->getCountNewMemberPackageInactive($dataUser);
        }
        if($dataUser->is_active == 1){
            $getCheckNewOrder = $modelMemberPackage->getCountMemberPackageInactive($dataUser);
        }
        $totalBonus = $modelBonus->getTotalBonusDashboardMember($dataUser);
        $totalWD = $modelWD->getTotalDiTransfer($dataUser);
        $getMyPackage = $modelPackage->getPackageId($dataUser->package_id);
        $stock_wd = 0;
        if($getMyPackage != null){
            $stock_wd = $getMyPackage->stock_wd;
        }
        
        
        $kanan_id = $dataUser->kanan_id;
        $kiri_id = $dataUser->kiri_id;
        $tengah_id = $dataUser->tengah_id;
        $getCekPlan = $modelMember->getActiveGenealogy($dataUser->id);
        if($getCekPlan != null){
            $kanan_id = $getCekPlan->kanan_id;
            $kiri_id = $getCekPlan->kiri_id;
            $tengah_id = $getCekPlan->tengah_id;
        }
        $kanan = 0;
        if($kanan_id != null){
            $downlineKanan = $dataUser->upline_detail.',['.$dataUser->id.']'.',['.$kanan_id.']';
            if($dataUser->upline_detail == null){
                $downlineKanan = '['.$dataUser->id.']'.',['.$kanan_id.']';
            }
            $kanan = $modelMember->getCountMyDownline($downlineKanan) + 1;
        }
        $kiri = 0;
        if($kiri_id != null){
            $downlineKiri = $dataUser->upline_detail.',['.$dataUser->id.']'.',['.$kiri_id.']';
            if($dataUser->upline_detail == null){
                $downlineKiri = '['.$dataUser->id.']'.',['.$kiri_id.']';
            }
            $kiri = $modelMember->getCountMyDownline($downlineKiri) + 1;
        }
        $tengah = 0;
        if($tengah_id != null){
            $downlineTengah = $dataUser->upline_detail.',['.$dataUser->id.']'.',['.$tengah_id.']';
            if($dataUser->upline_detail == null){
                $downlineTengah = '['.$dataUser->id.']'.',['.$tengah_id.']';
            }
            $tengah = $modelMember->getCountMyDownline($downlineTengah) + 1;
        }
        
        $plan = 'A';
        if($dataUser->plan == 2){
            $plan = 'B';
        }
        $totalCoin = $modelPin->getTotalCoinMember($dataUser);
        $dataDashboard = (object) array(
            'total_bonus' => floor($totalBonus->total_bonus),
            'total_wd' => $totalWD->total_wd,
            'total_tunda' => $totalWD->total_tunda,
            'total_fee_admin' => $totalWD->total_fee_admin,
            'fee_tuntas' => $totalWD->fee_tuntas,
            'fee_tunda' => $totalWD->fee_tunda,
            'stock_wd' => $stock_wd,
            'kanan' => $kanan,
            'kiri' => $kiri,
            'tengah' => $tengah,
            'member_tdk_aktif' => 0,
            'paket_name' => $getMyPackage->name,
            'plan' => $plan,
            'total_coin_masuk' => $totalCoin->sum_coin_masuk,
            'total_coin_keluar' => $totalCoin->sum_coin_keluar
        );
//        dd($dataDashboard);
        $mySales = 0;
        $image = '';
        $name = 'Member Biasa';
        $dataMy = (object) array(
            'name' => $name,
            'image' => $image,
            'sales' => $mySales
        );
        return view('member.home.dashboard')
                    ->with('headerTitle', 'Dashboard')
                    ->with('dataOrder', $getCheckNewOrder)
                    ->with('dataAll', $dataDashboard)
                    ->with('dataSponsor', $dataSponsor)
                    ->with('dataMy', $dataMy)
                    ->with('dataUser', $dataUser);
    }
    

}
