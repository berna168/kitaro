<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Model\Member;
use App\Model\Bonus;

class CronBonusLevel extends Command {

    protected $signature = 'bonus_level';
    protected $description = 'Cron Mingguan Bonus Level';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $modelMember = New Member;
        $modelBonus = new Bonus;
        $bonus_level = 1500;
        $getAllMember = $modelMember->getAllMemberData();
        foreach($getAllMember as $rowAllMember){
            if($rowAllMember->upline_detail != null){
                $uplineDetail = $rowAllMember->upline_detail;
                $arrayUplineId = explode(',', $uplineDetail);
                $dataUpline = array();
                $no = count($arrayUplineId) + 1;
                foreach($arrayUplineId as $rowUpline){
                    $no--;
                    $rm1 = str_replace('[', '', $rowUpline);
                    $rm2 = str_replace(']', '', $rm1);
                    $int = (int) $rm2;
                    $dataUpline[] = array(
                        'level' => $no,
                        'id' => $int
                    );
                }
                $dataUplineReverse = array_reverse($dataUpline);
                foreach($dataUplineReverse as $rowUpline){
                    if($rowUpline['level'] <= 6){
                        $dataInsertBonusLevel = array(
                            'user_id' => $rowUpline['id'],
                            'from_user_id' => $rowAllMember->id,
                            'type' => 3,
                            'bonus_price' => $bonus_level,
                            'bonus_date' => date('Y-m-d'),
                            'poin_type' => 1,
                            'level_id' => $rowUpline['level'],
                        );
                        $modelBonus->getInsertBonusMember($dataInsertBonusLevel);
                    }
                }
            }
        }
        dd('done bonus level ver 1');
    }
    
    
    
    
}
