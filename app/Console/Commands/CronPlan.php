<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Model\Member;
use App\Model\Bonus;

class CronPlan extends Command {

    protected $signature = 'plan {type}';
    protected $description = 'Cron Change Plan';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $modelMember = New Member;
        $modelBonus = new Bonus;
        $bonus_level = 10000;
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 1500);
        $plan = $this->argument('type');
        $aaaaa = array(1, 2, 3, 5, 6);
        if($plan == 1){
            $level_ke1 = pow(3, 1);
            $level_ke2 = pow(3, 2);
            $level_ke3 = pow(3, 3);
            $level_ke4 = pow(3, 4);
            $level_ke5 = pow(3, 5);
            $level_ke6 = pow(3, 6);
            $syarat_total_level = $level_ke1 + $level_ke2 + $level_ke3 + $level_ke4 + $level_ke5 + $level_ke6;
            $getData1 = $modelMember->getMemberUpgrdePlan($plan);
            if($getData1 != null){
                $bonus_reward_plan_a = 2000000;
                $cek = array();
                foreach($getData1 as $row1){
                    $downline1 = $row1->upline_detail.',['.$row1->id.']';
                    $arrayUpline = explode(',', $row1->upline_detail);
                    $pengurangan = count($arrayUpline);
                    if($row1->upline_detail == null){
                        $downline1 = '['.$row1->id.']';
                        $arrayUpline = null;
                        $pengurangan = 0;
                    }
                    $cekTotalDownline = $modelMember->getCekKakiLevelCount($downline1);
                    if($cekTotalDownline >= $syarat_total_level){
                        $getLevelDownline1 = $modelMember->getCekKakiLevel($downline1);
                        if($getLevelDownline1 != null){
                            $jml_level1 = $jml_level2 = $jml_level3 = $jml_level4 = $jml_level5 = $jml_level6 = array();
                            foreach($getLevelDownline1 as $rowDown1){
                                $jmlLevel = count(explode(',', $rowDown1->upline_detail));
                                if($jmlLevel - $pengurangan == 1){
                                    $total_lvl1 = $jmlLevel - $pengurangan;
                                    $jml_level1[] = array($total_lvl1);
                                }
                                if($jmlLevel - $pengurangan == 2){
                                    $total_lvl2 = $jmlLevel - $pengurangan;
                                    $jml_level2[] = array($total_lvl2);
                                }
                                if($jmlLevel - $pengurangan == 3){
                                    $total_lvl3 = $jmlLevel - $pengurangan;
                                    $jml_level3[] = array($total_lvl3);
                                }
                                if($jmlLevel - $pengurangan == 4){
                                    $total_lvl4 = $jmlLevel - $pengurangan;
                                    $jml_level4[] = array($total_lvl4);
                                }
                                if($jmlLevel - $pengurangan == 5){
                                    $total_lvl5 = $jmlLevel - $pengurangan;
                                    $jml_level5[] = array($total_lvl5);
                                }
                                if($jmlLevel - $pengurangan == 6){
                                    $total_lvl6 = $jmlLevel - $pengurangan;
                                    $jml_level6[] = array($total_lvl6);
                                }
                            }
                            $total_all = count($jml_level1) + count($jml_level2) + count($jml_level3) + count($jml_level4) + count($jml_level5) + count($jml_level6);
                            if($total_all >= $syarat_total_level){
                                //naik ke plan b
//                                $cek[] = array(
//                                    'user_id' => $row1->id,
//                                    'user_code' => $row1->user_code,
//                                    'total' => $total_all
//                                );
                                
                                $cekPlanB_upline = null;
                                if($arrayUpline != null){
                                    foreach($arrayUpline as $key=>$rowUpline){
                                        $arrayUpline[$key] = (int) str_replace('[', '', str_replace(']', '', $rowUpline));
                                    }
                                    $cekPlanB_upline = $modelMember->getCekUplinePlanB($arrayUpline);
                                }
                                
                                
                                //jadi dibawah si $cekPlanB_upline->id
                                if($cekPlanB_upline != null){
                                    //masuk ke struktur seseorang
                                    $newMemberUpline = $cekPlanB_upline->b_upline_detail.',['.$cekPlanB_upline->id.']';
                                    if($cekPlanB_upline->b_upline_detail == null){
                                        $newMemberUpline = '['.$cekPlanB_upline->id.']';
                                    }
                                    $dataInsertGeneaLogy = array(
                                        'user_id' => $row1->id,
                                        'upline_id' => $cekPlanB_upline->id,
                                        'upline_detail' => $newMemberUpline
                                    );
                                    $modelMember->getInsertGenealogy($dataInsertGeneaLogy);
                                    
                                    //update kiri-tengah-kanan upline-idnya
                                    $updatePosisi = 'kanan_id';
                                    if($cekPlanB_upline->b_tengah_id == null){
                                        $updatePosisi = 'tengah_id';
                                    }
                                    if($cekPlanB_upline->b_kiri_id == null){
                                        $updatePosisi = 'kiri_id';
                                    }
                                    $dataUpdatePosisi = array(
                                        $updatePosisi => $row1->id
                                    );
                                    $modelMember->getUpdateGenealogy('user_id', $cekPlanB_upline->id, $dataUpdatePosisi);
                                    
                                    //bonus Level keatas 3 tingkat
                                    $arrayUplineId = explode(',', $newMemberUpline);
                                    $dataUpline = array();
                                    $no = count($arrayUplineId) + 1;
                                    foreach($arrayUplineId as $rowUpline){
                                        $no--;
                                        $rm1 = str_replace('[', '', $rowUpline);
                                        $rm2 = str_replace(']', '', $rm1);
                                        $int = (int) $rm2;
                                        $dataUpline[] = array(
                                            'level' => $no,
                                            'id' => $int
                                        );
                                    }
                                    $dataUplineReverse = array_reverse($dataUpline);
                                    foreach($dataUplineReverse as $rowUpline){
                                        if($rowUpline['level'] <= 3){
                                            $dataInsertBonusLevel = array(
                                                'user_id' => $rowUpline['id'],
                                                'from_user_id' => $row1->id,
                                                'type' => 11,
                                                'bonus_price' => $bonus_level,
                                                'bonus_date' => date('Y-m-d'),
                                                'poin_type' => 1,
                                                'level_id' => $rowUpline['level'],
                                            );
                                            $modelBonus->getInsertBonusMember($dataInsertBonusLevel);
                                        }
                                    }
                                }
                                
                                //jadi top plan b
                                if($cekPlanB_upline == null){
                                    $dataInsertGeneaLogy = array(
                                        'user_id' => $row1->id,
                                    );
                                    $modelMember->getInsertGenealogy($dataInsertGeneaLogy);
                                }
                                
                                //ini sudah benar
                                $dataUpdateUser = array(
                                    'plan' => 2
                                );
                                $modelMember->getUpdateUsers('id', $row1->id, $dataUpdateUser);
                                $dataInsertRewardPlanA = array(
                                    'user_id' => $row1->id,
                                    'from_user_id' => $row1->id,
                                    'type' => 10,
                                    'bonus_price' => $bonus_reward_plan_a,
                                    'bonus_date' => date('Y-m-d'),
                                    'poin_type' => 1,
                                );
                                $modelBonus->getInsertBonusMember($dataInsertRewardPlanA);
                            }
                        }
                    }
                }
            }
//            dd($cek);
            dd('done upgrade plan-a to plan-b');
        }
        if($plan == 2){
            $getData2 = $modelMember->getMemberUpgrdePlan($plan);
            dd($getData2);
            //cari dr foreach ini 3 level kebawah yg sudah terisi
        }
        if($plan != 1 || $plan != 2){
            dd('stop here');
        }
    }
    
    
    
    
}
