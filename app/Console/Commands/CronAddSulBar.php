<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Model\Admin;
use App\Model\Pin;


class CronAddSulBar extends Command {

    protected $signature = 'add_sulbar';
    protected $description = 'Add Sulawesi Barat';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $modelAdmin = New Admin;
        $dataInsert = array(
            'kode' => '76.00.00.0000',
            'nama' => 'Sulawesi Barat',
            'propinsi' => 76,
            'kabupatenkota' => 00,
            'kecamatan' => 00,
            'kelurahan' => 0000
        );
        $modelAdmin->getInsertDaerah($dataInsert);
        dd('done insert sulawesi barat');
    }
    
    
    
    
}
