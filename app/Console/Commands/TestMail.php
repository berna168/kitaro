<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class TestMail extends Command {

    protected $signature = 'test_email';
    protected $description = 'Test Email';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $dataEmail = array(
            'dataEmail' => 'email'
        );
        $emailSend = 'chairil.hakim@domikado.com';
        Mail::send('member.email.testmail', $dataEmail, function($message) use($emailSend){
            $message->to($emailSend, 'Invitation for Interview')
                    ->subject('Invitation for Interview');
        });
        dd('done');
    }
    
    
    
    
}
