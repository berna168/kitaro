<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Model\Pin;
use App\Model\Pinsetting;


class CronAddMemberCoin extends Command {

    protected $signature = 'coin_setting';
    protected $description = 'Add Coin Setting';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $modelPin = New Pin;
        $modelPinSetting = New Pinsetting;
        $getLastSetting = $modelPinSetting->getActiveCoinSetting();
        $getData = $modelPin->get2020MemberCoin();
        if($getData != null){
            foreach($getData as $row){
                $dataUpdate = array(
                    'coin_setting_id' => $getLastSetting->id
                );
                $modelPin->getUpdateMemberCoin('id', $row->id, $dataUpdate);
            }
        }
        
        dd('done member coin setting');
    }
    
    
    
    
}
