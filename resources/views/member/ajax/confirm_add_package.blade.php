@if($setuju == 1)
<form class="login100-form validate-form" method="post" action="/m/add/package">
    {{ csrf_field() }}
    <div class="modal-header justify-content-center">
        <h4 class="title title-up">Konfirmasi Order Package</h4>
    </div>
    <div class="modal-body">
        <?php 
            $price = $getData->pin * $pinSetting->price;
            ?>
        <div class="col-md-12 pricing-table">
            <div class="pricing-item pricing-featured">
                <div class="selected">Membership</div>
                <div class="pricing-value">
                    <img src="/image/kitaro_logo.png" alt="kitaro_logo" style="width: 140px;">
                </div>
                <div class="pricing-title">
                    <h5><b>{{$getData->name}}</b></h5>
                    <h5><b>Rp. {{number_format($price, 0, ',', ',')}}</b></h5>
                    <h5><b>({{$getData->pin}} Pin)</b></h5>
                </div>
                <ul class="pricing-features">
                    <li>Bonus Sponsor Rp. 10.000 / Pin</li>
                    <li>Link Referal</li>
                </ul>
            </div>
        </div>
    </div>
    <input type="hidden" name="id_paket" value="{{$getData->id}}">
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary waves-effect waves-light">Order</button>
    </div>
</form>
@endif
@if($setuju == 0)
    <div class="modal-header justify-content-center">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Order Package</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <h4 class="text-danger" style="text-align: center;"> Anda belum menyetujui menyetujui Aturan dan Ketentuan Keanggotaan Kitaro Network </h4>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
    </div>
@endif