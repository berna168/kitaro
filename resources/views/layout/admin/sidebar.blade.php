<?php //SIDEBAR KIRI ?>
<div class="sidebar" data-color="brown" data-active-color="danger">
    <div class="logo">
        <a href="{{ URL::to('/') }}/adm/dashboard" class="simple-text logo-normal">
        &nbsp;&nbsp;
        Kitaro Admin
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            
            @if($dataUser->user_type == 2 || $dataUser->user_type == 1)
            <li class="@if(Route::currentRouteName() == 'addCrew') active @endif">
                <a href="{{ URL::to('/') }}/adm/add-admin">
                    <i class="nc-icon nc-circle-10"></i>
                    <p>Admin</p>
                </a>
            </li>
            @endif
            @if($dataUser->user_type == 3 || $dataUser->user_type == 2 || $dataUser->user_type == 1)
            <?php
                $can = $can2 = $can3 = $can4 = $can5 = $can6 = $can7 = true;
                if($dataUser->permission != null){
                    $cekCan = explode(',', $dataUser->permission);
                    if(!in_array('2', $cekCan)){
                        $can2 = false;
                    }
                    if(!in_array('3', $cekCan)){
                        $can3 = false;
                    }
                    if(!in_array('4', $cekCan)){
                        $can4 = false;
                    }
                    if(!in_array('5', $cekCan)){
                        $can5 = false;
                    }
                    if(!in_array('6', $cekCan)){
                        $can6 = false;
                    }
                    if(!in_array('7', $cekCan)){
                        $can7 = false;
                    }
                }
            ?>
            <li class="@if(Route::currentRouteName() == 'adm_listBonusSP') active @endif">
                <a data-toggle="collapse" href="#pageLaporanBonus" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-book-bookmark"></i>
                    <p>Laporan Bonus  <b class="caret"></b></p>
                </a>
                <div class="collapse @if(Route::currentRouteName() == 'adm_listBonusSP') show @endif" id="pageLaporanBonus">
                    <ul class="nav">
                        <li @if(Route::currentRouteName() == 'adm_listBonusSP') class="active" @endif>
                            <a href="{{ URL::to('/') }}/adm/list/bonus-sp">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Total Bonus Sponsor </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            
            @if($can2)
            <li class="@if(Route::currentRouteName() == 'adm_listWD' || Route::currentRouteName() == 'adm_listHistoryWD') active @endif">
                <a data-toggle="collapse" href="#pageLaporanWD" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-money-coins"></i>
                    <p>Withdrawal  <b class="caret"></b></p>
                </a>
                <div class="collapse @if(Route::currentRouteName() == 'adm_listWD' || Route::currentRouteName() == 'adm_listHistoryWD') show @endif" id="pageLaporanWD" style="">
                    <ul class="nav">
                        <li @if(Route::currentRouteName() == 'adm_listWD') class="active" @endif>
                            <a href="{{ URL::to('/') }}/adm/list/wd">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Request Withdrawal </span>
                            </a>
                        </li>
                        <?php
                        /*
<!--                        <li>
                            <a href="{{ URL::to('/') }}/adm/list/wd-eidr">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Request Konversi eIDR </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/adm/list/claim-reward">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Claim Reward </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/adm/list/belanja-reward">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Claim Belanja Reward </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/adm/list/penjualan-reward">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Claim Penjualan Reward </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/adm/list/wd-royalti">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Request Withdrawal Royalti </span>
                            </a>
                         </li>-->
                         */
                        ?>
                         <li @if(Route::currentRouteName() == 'adm_listHistoryWD') class="active" @endif>
                            <a href="{{ URL::to('/') }}/adm/history/wd">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> History Withdrawal </span>
                            </a>
                        </li>
                        <?php
                        /*
<!--                        <li>
                            <a href="{{ URL::to('/') }}/adm/history/wd-eidr">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> History Konversi eIDR </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/adm/history/claim-reward">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> History Claim Reward </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/adm/history/belanja-reward">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> History Belanja Reward </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/adm/history/penjualan-reward">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> History Penjualan Reward </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/adm/history/wd-royalti">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> History Withdrawal Royalti</span>
                            </a>
                        </li>-->
                         */
                        ?>
                    </ul>
                </div>
            </li>
            @endif
            
            @if($can3)
            <li class="@if(Route::currentRouteName() == 'adm_listTransaction' || Route::currentRouteName() == 'adm_listHistoryTransaction') active @endif">
                <a data-toggle="collapse" href="#pagePin" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-bank"></i>
                    <p>Pin  <b class="caret"></b></p>
                </a>
                <div class="collapse @if(Route::currentRouteName() == 'adm_listTransaction' || Route::currentRouteName() == 'adm_listHistoryTransaction') show @endif" id="pagePin" style="">
                    <ul class="nav">
                        <li @if(Route::currentRouteName() == 'adm_listTransaction') class="active" @endif>
                            <a href="{{ URL::to('/') }}/adm/list/transactions">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Data Order Pin </span>
                            </a>
                        </li>
                        <li @if(Route::currentRouteName() == 'adm_listHistoryTransaction') class="active" @endif>
                            <a href="{{ URL::to('/') }}/adm/history/transactions">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> History Order Pin </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            @endif
            
            @if($can4)
            <li class="@if(Route::currentRouteName() == 'adm_listMember') active @endif">
                <a data-toggle="collapse" href="#pageMember" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-single-02"></i>
                    <p>Member  <b class="caret"></b></p>
                </a>
                <div class="collapse @if(Route::currentRouteName() == 'adm_listMember') show @endif" id="pageMember" style="">
                    <ul class="nav">
                        <li @if(Route::currentRouteName() == 'adm_listMember') class="active" @endif>
                            <a href="{{ URL::to('/') }}/adm/list/member">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> List Member </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            @endif
            
            @if($can7)
            <li class="@if(Route::currentRouteName() == 'addSettingPin' || Route::currentRouteName() == 'allPackage' || Route::currentRouteName() == 'adm_bankPerusahaan' || Route::currentRouteName() == 'adm_bonusStart' || Route::currentRouteName() == 'addSettingCoin') active @endif">
                <a data-toggle="collapse" href="#pagesExamples" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-settings-gear-65"></i>
                    <p>Setting  <b class="caret"></b></p>
                </a>
                <div class="collapse @if(Route::currentRouteName() == 'addSettingPin' || Route::currentRouteName() == 'allPackage' || Route::currentRouteName() == 'adm_bankPerusahaan' || Route::currentRouteName() == 'adm_bonusStart' || Route::currentRouteName() == 'addSettingCoin') show @endif" id="pagesExamples" style="">
                    <ul class="nav">
                        <li @if(Route::currentRouteName() == 'addSettingPin') class="active" @endif>
                            <a href="{{ URL::to('/') }}/adm/add/pin-setting">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Pin </span>
                            </a>
                        </li>
                        <li @if(Route::currentRouteName() == 'allPackage') class="active" @endif>
                            <a href="{{ URL::to('/') }}/adm/packages">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Package </span>
                            </a>
                        </li>
                        <li @if(Route::currentRouteName() == 'adm_bankPerusahaan') class="active" @endif>
                            <a href="{{ URL::to('/') }}/adm/bank">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Bank Perusahaan </span>
                            </a>
                        </li>
                        <li @if(Route::currentRouteName() == 'adm_bonusStart') class="active" @endif>
                            <a href="{{ URL::to('/') }}/adm/bonus-start">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Bonus Start </span>
                            </a>
                        </li>
                        <li @if(Route::currentRouteName() == 'addSettingCoin') class="active" @endif>
                            <a href="{{ URL::to('/') }}/adm/add/coin-setting">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Coin Exchange </span>
                            </a>
                        </li>
<!--                        <li>
                            <a href="{{ URL::to('/') }}/adm/bonus-reward">
                            <span class="sidebar-mini-icon">+</span>
                            <span class="sidebar-normal"> Bonus Reward </span>
                            </a>
                        </li>-->
                    </ul>
                </div>
            </li>
            @endif
            
            @endif
            
            <li>
                <a href="{{ URL::to('/') }}/user_logout">
                    <i class="nc-icon nc-button-power text-danger"></i>
                    <p>Log Out</p>
                </a>
            </li>
        </ul>
    </div>
</div>