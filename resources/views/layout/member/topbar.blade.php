<div class="topbar">
    <div class="topbar-left" style="background-color: #ffc46c;">
        <a class="logo" href="{{ URL::to('/') }}/adm/dashboard">
            <img src="/image/kitaro_header.png">
        </a>
    </div>
    <nav class="navbar navbar-custom" style="background-color: #FF9900;">
        <ul class="nav navbar-nav">
            <li class="nav-item">
                <button class="button-menu-mobile open-left waves-light waves-effect">
                <i class="zmdi zmdi-view-headline"></i>
                </button>
            </li>
        </ul>
        <ul class="nav navbar-nav pull-right">
            
        </ul>
    </nav>
</div>