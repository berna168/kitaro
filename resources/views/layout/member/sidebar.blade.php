<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <div id="sidebar-menu">
            <ul>
                <li class="has_sub">
                    <a href="{{ URL::to('/') }}/m/dashboard" class="waves-effect @if(Route::currentRouteName() == 'mainDashboard') active @endif">
                        <i class="zmdi zmdi-view-dashboard" style="color: #ffc46c;"></i>
                        <span> Dashboard </span> 
                    </a>
                </li>
                <li class="has_sub">
                    <a class="waves-effect @if(Route::currentRouteName() == 'm_myProfile' || Route::currentRouteName() == 'm_newProfile' || Route::currentRouteName() == 'm_myBank' || Route::currentRouteName() == 'm_myTron' || Route::currentRouteName() == 'm_editPassword') active @endif">
                        <i class="zmdi zmdi-account-circle" style="color: #ffc46c;"></i> 
                        <span> User Profile</span> <span class="menu-arrow"></span>
                    </a>
                    <ul class="list-unstyled">
                        <li @if(Route::currentRouteName() == 'm_myProfile' || Route::currentRouteName() == 'm_newProfile') class="active" @endif><a href="{{ URL::to('/') }}/m/profile">Profile</a></li>
                        <li @if(Route::currentRouteName() == 'm_myBank') class="active" @endif><a href="{{ URL::to('/') }}/m/bank">Bank</a></li>
                        <li @if(Route::currentRouteName() == 'm_editPassword') class="active" @endif><a href="{{ URL::to('/') }}/m/edit/password">Ganti Password</a></li>
                        <li @if(Route::currentRouteName() == 'm_myTron') class="active" @endif><a href="{{ URL::to('/') }}/m/coin">Bull Coin</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a class="waves-effect @if(Route::currentRouteName() == 'm_newSponsor' || Route::currentRouteName() == 'm_addPlacement' || Route::currentRouteName() == 'm_statusMember' || Route::currentRouteName() == 'm_newRO') active @endif">
                        <i class="zmdi zmdi-accounts" style="color: #ffc46c;"></i> 
                        <span> Member </span> <span class="menu-arrow"></span>
                    </a>
                    <ul class="list-unstyled">
                        @if($dataUser->plan == 1)
                        <li @if(Route::currentRouteName() == 'm_newSponsor') class="active" @endif><a href="{{ URL::to('/') }}/m/add/sponsor">Daftar</a></li>
                        <li @if(Route::currentRouteName() == 'm_addPlacement') class="active" @endif><a href="{{ URL::to('/') }}/m/add/placement">Placement</a></li>
                        @endif
                        <li @if(Route::currentRouteName() == 'm_statusMember') class="active" @endif><a href="{{ URL::to('/') }}/m/status/member">Status</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a class="waves-effect @if(Route::currentRouteName() == 'm_statusSponsor' || Route::currentRouteName() == 'm_myTrinary' || Route::currentRouteName() == 'm_mySponsorTree') active @endif">
                        <i class="zmdi zmdi-device-hub" style="color: #ffc46c;"></i> 
                        <span> Networking </span> <span class="menu-arrow"></span>
                    </a>
                    <ul class="list-unstyled">
                        <li @if(Route::currentRouteName() == 'm_statusSponsor') class="active" @endif><a href="{{ URL::to('/') }}/m/status/sponsor">Sponsor</a></li>
                        <li @if(Route::currentRouteName() == 'm_myTrinary') class="active" @endif><a href="{{ URL::to('/') }}/m/my/trinary">Trinary Tree</a></li>
                        <li @if(Route::currentRouteName() == 'm_mySponsorTree') class="active" @endif><a href="{{ URL::to('/') }}/m/my/sponsor-tree">Sponsor Tree</a></li>
                    </ul>
                </li>
                @if($dataUser->plan == 1)
                <li class="has_sub">
                    <a class="waves-effect @if(Route::currentRouteName() == 'm_newPin' || Route::currentRouteName() == 'm_listTransactions' || Route::currentRouteName() == 'm_addTransferPin' 
                       || Route::currentRouteName() == 'm_myPinStock' || Route::currentRouteName() == 'm_myPinHistory' || Route::currentRouteName() == 'm_addTransaction') active @endif">
                        <i class="zmdi zmdi-archive" style="color: #ffc46c;"></i> 
                        <span> Pin </span> <span class="menu-arrow"></span>
                    </a>
                    <ul class="list-unstyled">
                        <li @if(Route::currentRouteName() == 'm_newPin') class="active" @endif><a href="{{ URL::to('/') }}/m/add/pin">Beli</a></li>
                        <li @if(Route::currentRouteName() == 'm_listTransactions' || Route::currentRouteName() == 'm_addTransaction') class="active" @endif><a href="{{ URL::to('/') }}/m/list/transactions">Transaksi</a></li>
                        <li @if(Route::currentRouteName() == 'm_addTransferPin') class="active" @endif><a href="{{ URL::to('/') }}/m/add/transfer-pin">Transfer</a></li>
                        <li @if(Route::currentRouteName() == 'm_myPinStock') class="active" @endif><a href="{{ URL::to('/') }}/m/pin/stock">Stock</a></li>
                        <li @if(Route::currentRouteName() == 'm_myPinHistory') class="active" @endif><a href="{{ URL::to('/') }}/m/pin/history">History</a></li>
                    </ul>
                </li>
                @endif
                <li class="has_sub">
                    <a class="waves-effect @if(Route::currentRouteName() == 'm_myBonusSummary' || Route::currentRouteName() == 'm_myBonusSponsor' || Route::currentRouteName() == 'm_myBonusLevel') active @endif">
                        <i class="zmdi zmdi-card-giftcard" style="color: #ffc46c;"></i> 
                        <span> Bonus </span> <span class="menu-arrow"></span>
                    </a>
                    <ul class="list-unstyled">
                        <li @if(Route::currentRouteName() == 'm_myBonusSummary') class="active" @endif><a href="{{ URL::to('/') }}/m/summary/bonus">Ringkasan</a></li>
                        <li @if(Route::currentRouteName() == 'm_myBonusSponsor') class="active" @endif><a href="{{ URL::to('/') }}/m/sponsor/bonus">Sponsor</a></li>
                        <li @if(Route::currentRouteName() == 'm_myBonusLevel') class="active" @endif><a href="{{ URL::to('/') }}/m/level/bonus">Level</a></li>
                        <?php
                        /*
                        <li @if(Route::currentRouteName() == 'm_myBonusBinary') class="active" @endif><a href="{{ URL::to('/') }}/m/binary/bonus">Binary</a></li>
                        <li @if(Route::currentRouteName() == 'm_requestClaimReward') class="active" @endif><a href="{{ URL::to('/') }}/m/req/claim-reward">Reward</a></li>
                         */
                        ?>
                    </ul>
                </li>
<!--                <li class="has_sub">
                    <a class="waves-effect @if(Route::currentRouteName() == 'm_myBonusSaldo' || Route::currentRouteName() == 'm_requestWDeIDR') active @endif">
                        <i class="zmdi zmdi-money-box" style="color: #ffc46c;"></i> 
                        <span> Saldo </span> <span class="menu-arrow"></span>
                    </a>
                    <ul class="list-unstyled">
                        <li @if(Route::currentRouteName() == 'm_myBonusSaldo') class="active" @endif><a href="{{ URL::to('/') }}/m/saldo/bonus">Bonus</a></li>
                        <?php //<li @if(Route::currentRouteName() == 'm_requestWDeIDR') class="active" @endif><a href="{{ URL::to('/') }}/m/req/wd-eidr">eIDR</a></li> ?>
                    </ul>
                </li>-->
                <li class="has_sub">
                    <a class="waves-effect @if(Route::currentRouteName() == 'm_requestWD' || Route::currentRouteName() == 'm_historyWD') active @endif">
                        <i class="zmdi zmdi-money" style="color: #ffc46c;"></i> 
                        <span> Withdrawal </span> <span class="menu-arrow"></span>
                    </a>
                    <ul class="list-unstyled">
                        <li @if(Route::currentRouteName() == 'm_historyWD') class="active" @endif><a href="{{ URL::to('/') }}/m/history/wd">History WD</a></li>
                        <li @if(Route::currentRouteName() == 'm_requestWD') class="active" @endif><a href="{{ URL::to('/') }}/m/req/wd">Request WD</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="{{ URL::to('/') }}/user_logout" class="waves-effect">
                        <i class="zmdi zmdi-power text-danger"></i>
                        <span> Logout </span> 
                    </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>